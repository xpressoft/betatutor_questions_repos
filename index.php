<?php
//Connect to the db
include "init.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
     
    <title>BetaTutor Questions Reposistory</title>
</head>
<body>
	<center>
	<h3>BetaTutor Questions Reposistory</h3>
	<hr/>
    <div>Insert Query | <a href="fetch_question.php"> Fetch Query</a></div>
	<br/>
	<form method="POST">
		<!-- exam type -->
		<b>Select Examtype:</b>
		<select name="type">
			<option value="------">------</option>
			<option value="utme">utme</option>
			<option value="wassce">wassce</option>
			<option value="post-utme">post-utme</option>
		</select>

		<b>|</b>

		<b>Select Subject:</b>
		<select name="subject">
			<option value="------">------</option>
			<option value="english">English language</option>
			<option value="mathematics ">Mathematics</option>
			<option value="commerce">Commerce</option>
			<option value="accounting">Accounting</option>
			<option value="biology">Biology</option>
			<option value="physics">Physics</option>
			<option value="chemistry">Chemistry</option>
			<option value="englishlit">English literature</option>
			<option value="government">Government</option>
			<option value="crk">Christian Religious Knowledge</option>
			<option value="geography">Geography</option>
			<option value="economics">Economics</option>
			<option value="irk">Islamic Religious Knowledge</option>
			<option value="civiledu">Civic Education</option>
			<option value="insurance">Insurance</option>
			<option value="currentaffairs">Current Affairs</option>
			<option value="history">History</option>
		</select>

		<b>|</b>

		<b>Select Year:</b>
		<select name="year">
			<option value="------">------</option>
			<option value="2001">2001</option>	
			<option value="2002">2002</option>	
			<option value="2003">2003</option>	
			<option value="2004">2004</option>	
			<option value="2005">2005</option>	
			<option value="2006">2006</option>	
			<option value="2007">2007</option>	
			<option value="2009">2008</option>	
			<option value="2010">2010</option>	
			<option value="2011">2011</option>	
			<option value="2012">2012</option>	
			<option value="2013">2013</option>	
			<option value="2014">2014</option>	
			<option value="2015">2015</option>	
			<option value="2016">2016</option>	
			<option value="2017">2017</option>	
			<option value="2018">2018</option>	
			<option value="2019">2019</option>	
			<option value="2020">2020</option>	
			<option value="2021">2021</option>	
			<option value="2022">2022</option>	
		</select>
  <br><br>
  <div><input type="submit" value="Copy" name="submit"></div>
  	<?php
		// if(isset($_POST['submit'])){
			// $subject = $_POST['subject'];
			// $year = $_POST['year'];
			// $type = $_POST['type'];

			$subject = 'commerce';
			$year = '2004';
			$type = 'utme';
			
			// if ($subject == "------" && $year == "------" && $type == "------"){
			// 	echo 'Wrong Parameters';
			// }else{
				// Using php cURL
				// Get cURL resource
				$curl = curl_init();

				// set option
				curl_setopt($curl, CURLOPT_URL, 'https://questions.aloc.ng/api/q/40?subject='.$subject.'&year='.$year.'&type='.$type.'');



				// return transfer set 1
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

				//include header
				curl_setopt($curl, CURLOPT_HEADER, 0);

				//execute
				$output = curl_exec($curl);

				if ($output === FALSE){
					echo "curl Error: ". curl_error($curl);
				}
				// Close request to clear up some resources
				curl_close($curl);

				// convert output to json
				$arr = json_decode($output, true);

				foreach ($arr['data'] as $element) {
					//initialize answer as empty string
					$answer = '';
					//get question value
					$question = $element['question'];
					$examyear = $element['examyear'];
					//assign_options
					$option_a = '';
					$option_b = '';
					$option_c = '';
					$option_d = '';
					//check for repited question
					$searcher = mysqli_query($con, "SELECT * FROM questions WHERE question='".$question."'") or die(mysql_errno());
					//count the total search and check if => 1
					$count = mysqli_num_rows($searcher);
					
					if ($count == 0) {
						foreach($element['option'] as $eId => $eValue){
							//echo $eId. ' : '. $eValue. '<br/>';
								switch ($eId) {
									case "a":
										$option_a = $eValue;
										break;
									case "b":
										$option_b = $eValue;
										break;
									case "c":
										$option_c = $eValue;
										break;
									case "d":
										$option_d = $eValue;
										break;
									default:
										//Null	
							}

							if($eId == $element['answer']){
								$answer = $eValue;
							}
						} 
						// insert questions query
						$sql_insert_question  = "INSERT INTO `questions`(`question`, `option_a`, `option_b`, `option_c`, `option_d`, `answer`, `subject`, `examyear`, `examtype`) VALUES ('".$question."','".$option_a."','".$option_b."','".$option_c."','".$option_d."','".$answer."','".$subject."','".$examyear."','".$type."')";
							if (mysqli_query($con, $sql_insert_question)) {
							   echo "New record inserted successfully <br/>";
							} else {
							   echo "Error: " . $sql_insert_question . "" . mysqli_error($con);
							}
							
					
					} else continue;
					
					
				}
							// }
			// }
	?>
	</form>
     </center>
</body>
</html>
<?php

//print_r($output);
mysqli_close($con);
?>