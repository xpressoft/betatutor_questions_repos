<?php
//Connect to the db
include "init.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
     
    <title>Fetch Query - BetaTutor Questions Reposistory</title>
</head>
<body>
	<center>
	<h3>BetaTutor Questions Reposistory</h3>
	<hr/>
    <div><a href="index.php"> Insert Query</a> |  Fetch Query</div><br/>
	
	<form method="POST">
		<!-- exam type -->
		<b>Select Examtype:</b>
		<select name="type">
			<option value="------">------</option>
			<option value="utme">utme</option>
			<option value="wassce">wassce</option>
			<option value="post-utme">post-utme</option>
		</select>

		<b>|</b>

		<b>Select Subject:</b>
		<select name="subject">
			<option value="------">------</option>
			<option value="english">English language</option>
			<option value="mathematics ">Mathematics</option>
			<option value="commerce">Commerce</option>
			<option value="accounting">Accounting</option>
			<option value="biology">Biology</option>
			<option value="physics">Physics</option>
			<option value="chemistry">Chemistry</option>
			<option value="englishlit">English literature</option>
			<option value="government">Government</option>
			<option value="crk">Christian Religious Knowledge</option>
			<option value="geography">Geography</option>
			<option value="economics">Economics</option>
			<option value="irk">Islamic Religious Knowledge</option>
			<option value="civiledu">Civic Education</option>
			<option value="insurance">Insurance</option>
			<option value="currentaffairs">Current Affairs</option>
			<option value="history">History</option>
		</select>

		<b>|</b>

		<b>Select Year:</b>
		<select name="year">
			<option value="------">------</option>
			<option value="2001">2001</option>	
			<option value="2002">2002</option>	
			<option value="2003">2003</option>	
			<option value="2004">2004</option>	
			<option value="2005">2005</option>	
			<option value="2006">2006</option>	
			<option value="2007">2007</option>	
			<option value="2009">2008</option>	
			<option value="2010">2010</option>	
			<option value="2011">2011</option>	
			<option value="2012">2012</option>	
			<option value="2013">2013</option>	
			<option value="2014">2014</option>	
			<option value="2015">2015</option>	
			<option value="2016">2016</option>	
			<option value="2017">2017</option>	
			<option value="2018">2018</option>	
			<option value="2019">2019</option>	
			<option value="2020">2020</option>	
			<option value="2021">2021</option>	
			<option value="2022">2022</option>	
		</select>
  <br><br>
  <div><input type="submit" value="Fetch" name="submit"></div>
  	<?php
		if(isset($_POST['submit'])){
			$subject = $_POST['subject'];
			$year = $_POST['year'];
			$type = $_POST['type'];
			
			if ($subject == "------" && $year == "------"){
				echo 'Wrong Parameters';
			}else{
				  $sql = 'SELECT * FROM `questions` WHERE `subject` = "'.$subject.'"';
				  //$sql = 'SELECT * FROM questions';
					$result = mysqli_query($con, $sql);

					if($result){
						 if (mysqli_num_rows($result) > 0) {
							while($row = mysqli_fetch_assoc($result)) {
							   ?>
							<p>Question q<?php echo $row['id']; ?> = new Question("<?php echo $row['question']; ?>", "<?php echo $row['option_a']; ?>", "<?php echo $row['option_b']; ?>", "<?php echo $row['option_c']; ?>", "<?php echo $row['option_d']; ?>", "<?php echo $row['answer']; ?>");<br/>
									this.addQuestion(q<?php echo $row['id']; ?>, <?php echo $row['subject']; ?>_TABLE_QUESTION);	</p>						   
							   <?php
							}
					 } else {
						echo "0 results";
					 }
					}else{
						echo "Error: " . $sql_insert_question . "" . mysqli_error($con);
					}

				
			}
	
		}
	?>
	</form>
     
</body>
</html>
<?php

//print_r($output);
mysqli_close($con);
?>